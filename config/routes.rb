Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html


    root    'profiles#show_my_profile'

            #UserController
    get 'user_signup'    => 'users#new'
    
    post 'user_login'    => 'users#login'

    delete 'user_logout' => 'users#destroy'

    get 'user_logout' => 'users#destroy'




    get "show_my_profile" => "profiles#show_my_profile"


    get 'users/:id/:tab' => 'profiles#show_tabs'
    

    #Issues
    get 'add_issue_view' => 'issues#add_issue_view'
    post 'add_issue' => 'issues#add_issue'
    post 'add_issue_comment' => 'issues#add_issue_comment'

    get 'issues/:id/' => 'issues#open_issue'

    post 'user_close_ticket' => 'issues#user_close_ticket'
    post 'user_reopen_ticket' => 'issues#user_reopen_ticket'
    post 'agent_close_ticket' => 'issues#agent_close_ticket'
    post 'agent_assign_ticket' => 'issues#agent_assign_ticket'



    get 'admin/:name' => 'profiles#admin_view'
    post 'user_suspend' => 'profiles#user_suspend'

    get 'generate_report' => 'profiles#generate_report'

    




    

    resources :users  


end
