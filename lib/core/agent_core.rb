require "core/core"
class AgentCore < Core
	def initialize agent
		@agent=agent
	end

	def get_open_cases page_num
		@updates=Ticket.where(assigned_to: @agent.id, state:1)

	end

	def get_new_cases page_num
		@updates=Ticket.where(state:0)

	end

	def get_closed_cases page_num
		@updates=Ticket.where(assigned_to: @agent.id, state:2)

	end

	def get_all_cases page_num
		@updates=Ticket.where(assigned_to: @agent.id)

	end




end