require "core/core"
class UserCore < Core
	def initialize user
		@user=user
	end

	def get_open_cases page_num
		#debugger
		@updates=Ticket.where(created_by: @user.id, state:1)

	end

	def get_new_cases page_num
		@updates=Ticket.where(created_by: @user.id, state:0)

	end

	def get_closed_cases page_num
		@updates=Ticket.where(created_by: @user.id, state:2)

	end

	def get_all_cases page_num
		@updates=Ticket.where(created_by: @user.id)

	end



end