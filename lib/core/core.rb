class Core

	def get_data page, page_num
		#debugger
		case page
			when 'open'
				data=get_open_cases page_num
			when 'new'
				data=get_new_cases page_num
			when 'closed'
				data=get_closed_cases page_num
			when 'all'
				data=get_all_cases page_num

		end
		return data
	end

end