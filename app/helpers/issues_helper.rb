module IssuesHelper

    def get_userlist comments
    	begin
	    	user_ids=comments.pluck(:user_id)

	    	users=User.where("id in (?)",user_ids)

	    	@user_map={}
	    	users.each do |user|
	    		@user_map[user.id]=user
	    	end

	    rescue
	    	return {} 
	    end


    end

end
