
require "core/user_core"
require "core/agent_core"
class User < ApplicationRecord
	before_save{self.email=email.downcase}
	validates :username, presence:  true, length:{maximum:255},uniqueness: {case_sensitive:false}
	validates :email, presence: true, length:{maximum:255}, uniqueness: {case_sensitive:false}
	has_secure_password
	validates :password, length:{minimum:8}

    mount_uploader :profilepic, LogoUploaderUploader



    def anonymous_profilepic
        return "user_no_image5.jpg"
    end

    def get_profile_pic
		if profilepic && profilepic.length>0 then 
			return profilepic
		end
		if self.user?
			return "user_no_image5.jpg" 
		else
			return "user_no_image6.png" 
		end

	end
    def get_profile_pic_url
		if profilepic && profilepic.length>0 then 
			return profilepic.url
		end
        return nil
	end


	#This accessor will have a unique string and the database will have a digest version of this string for 
	#the user who is logged in currently
	attr_accessor :remember_token


    	#This can execute any sql and give result as Mysql2::Result. NEVER CALL THIS FUNCTION
	def self.executesql(sql)
		
		records_array = ActiveRecord::Base.connection.execute(sql)
	end
	# Returns the hash digest of the given string.
	def digest(string)
		cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST : BCrypt::Engine.cost
		BCrypt::Password.create(string, cost: cost)
	end

	# Returns a random string token.
	def new_token
		SecureRandom.urlsafe_base64
	end

	#Saves the remember token in db
	def remember
		#This creates and assignes a new string token into the accessor
		self.remember_token = new_token
		#We can call update_attribute directly becoz we are alredy in the Model object
		#update_attribute can change db itemw for users without passing in a password
		update_attribute(:remember_digest, digest(remember_token))
	end

	def get_remembertoken
		return self.remember_token
	end

	# Returns true if the given token matches the digest.
	def authenticated?(remember_token)
		#if logged in from multiple browsers, and logged out from 1, this cud be null
		return false if remember_digest.nil?
		#hashes the token and compares it with digest
		BCrypt::Password.new(remember_digest).is_password?(remember_token)# you can use == also in place of is_password
	end

	# Forgets a user.
	def forget
		update_attribute(:remember_digest, nil)
	end

	def user?
		return true if user_type==0
	end	
	def agent?
		return true if user_type==1
	end	
	def admin?
		return true if user_type==2
	end

	def get_user_object
		if user?
			obj=UserCore.new self
		elsif agent?
			obj=AgentCore.new self
		end
	end

	def get_updates page, page_num
		obj=get_user_object 
		obj.get_data page, page_num
		
	end

	def toggle_suspension
		newval=(self.active==0 ) ? 1 : 0
		self.update_attribute('active', newval)
	end

	def suspended?
		return true unless self.active==1
	end
end
