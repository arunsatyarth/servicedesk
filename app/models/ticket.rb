class Ticket < ApplicationRecord
	def new?
		return true if self.state==0
	end	

	def open?
		return true if self.state==1
	end	

	def closed?
		return true if self.state==2
	end	

	def close
		self.state=2
		save
	end

	def open user_id
		self.state=1
		self.assigned_to=user_id
		save
	end
	def reopen
		if self.assigned_to.nil? 
			self.state=0
		else
			self.state=1
		end
		save
	end
end
