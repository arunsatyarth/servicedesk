class IssuesController < ApplicationController
include MarkdownHelper
   include UsersHelper
   include IssuesHelper

    def add_issue_view
        @type="add_issue_view"
        respond_to do |format|
            format.js {render :file => "issues/issues.js.erb" }
        end
    end
    
    def add_issue
        @success=false
        begin
            throw "not logged in" unless current_user
            heading=params[:data][:heading]
            content=params[:data][:content]

            parsed_content=Markdown.render(content)
            if heading.length>=3
                @ticket=Ticket.new(created_by: current_user.id,heading:heading)
                @success=@ticket.save
                if @success
					actualcontent=ActualContent.new(content: content)
                	actualcontent.save

                	@ticket_data=TicketDatum.new(ticket_id:@ticket.id, content: parsed_content,
                		actual_content_id: actualcontent.id)
                	@ticket_data.save
                	

                end
            end
       	rescue Exception=> ex
            #debugger

        end

        @type="add_issue"
        respond_to do |format|
            format.js {render :file => "issues/issues.js.erb" }
        end
    end

    def add_issue_comment
        @success=false
        begin
            throw "not logged in" unless current_user
            comment=params[:comm][:content]
            ticket_id=params[:id]

            @ticket=Ticket.find_by(id:ticket_id);

            if comment.length>=1 && @ticket
                add_comment @ticket, comment
            end
        rescue Exception=> ex
            debugger

        end

        @type="add_issue_comment"
        respond_to do |format|
            format.js {render :file => "issues/issues.js.erb" }
        end
    end

    def open_issue
      	@success=false
        begin
            throw "not logged in" unless current_user
            id=params[:id]

            @ticket=Ticket.find_by(id:id);
            if( @ticket && access( @ticket))
            	open_issue_data id
            	@success=true
                if current_user.agent?
                    @agent_list=User.where(user_type:1).pluck(:username)
                end
            end
       	rescue Exception=> ex
            #debugger

        end

        @type="open_issue"
        respond_to do |format|
            format.js {render :file => "issues/issues.js.erb" }
            format.html do 
                render "users/show"
            end
        end
    end

    def user_close_ticket
        begin
            throw "not logged in" unless current_user
            id=params[:id]

            @ticket=Ticket.find_by(id:id);
            if( @ticket && access( @ticket))
                @ticket.close
                add_comment @ticket, "Ticket Closed"


                open_issue_data id
            end
        rescue Exception=> ex
        end

        @type="open_issue"
        respond_to do |format|
            format.js {render :file => "issues/issues.js.erb" }
        end 
    end

    def user_reopen_ticket
        begin
            throw "not logged in" unless current_user
            id=params[:id]

            @ticket=Ticket.find_by(id:id);
            if( @ticket && access( @ticket))
                @ticket.reopen
                add_comment @ticket, "Ticket Reopened"

                open_issue_data id
            end
        rescue Exception=> ex
        end

        @type="open_issue"
        respond_to do |format|
            format.js {render :file => "issues/issues.js.erb" }
        end 
    end

    def agent_close_ticket
        begin
            throw "not logged in" unless current_user
            id=params[:id]

            @ticket=Ticket.find_by(id:id);
            if( @ticket && access( @ticket) )
                @ticket.close
                add_comment @ticket, "Ticket Closed by agent"

                open_issue_data id
            end
        rescue Exception=> ex
        end

        @type="open_issue"
        respond_to do |format|
            format.js {render :file => "issues/issues.js.erb" }
        end 
    end

    def agent_assign_ticket
        begin
            throw "not logged in" unless current_user
            id=params[:id]
            username=params[:info][:agent_name]
            user=get_assigned_person username

            throw "not assignable " unless assignable user

            @ticket=Ticket.find_by(id:id);
            if( @ticket && access( @ticket) )
                @ticket.open user.id
                add_comment( @ticket, "Ticket Assigned to agent " +user.username)

                open_issue_data id
            end
        rescue Exception=> ex
        end

        @type="open_issue"
        respond_to do |format|
            format.js {render :file => "issues/issues.js.erb" }
        end 
    end


    private

        def get_assigned_person username
           begin
                if current_user.username==username
                    return current_user
                else
                    user=User.find_by(username:username)
                    return user
                end
            rescue
                return nil
            end 
        end

        def assignable user
            begin
                return true if user.agent?
            rescue
                return false
            end
        end

        def add_comment ticket, comment
            parsed_content=Markdown.render(comment)

            actualcontent=ActualContent.new(content: comment)
            @success=   actualcontent.save
            
            if @success
                @comment=Comment.new(ticket_id: ticket.id,user_id: current_user.id,comment:parsed_content,actual_content_id: actualcontent.id)
                @success=@comment.save
            end
            comments=[]
            comments.push @comment
            get_userlist comments
        end

        def open_issue_data id
            @ticketdata=TicketDatum.find_by(ticket_id: id)
            @comments=Comment.where(ticket_id: id).limit(30)

            @user_list=get_userlist @comments
        end

        def access ticket
            if current_user.user? 
                return ticket.created_by==current_user.id#if user only created guy can see
            else
                if ticket.new? 
                    return true#any agent can see new issue
                else
                    return @ticket.assigned_to==current_user.id #only agent working on it can see
                end

            end
        end
end
