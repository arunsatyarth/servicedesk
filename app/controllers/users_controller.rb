class UsersController < ApplicationController
   include UsersHelper
    include ProfilesHelper

PAGINATION_PERPAGE=10
    
  #Shows the page to login and signup
  def new
    #When login fails, it comes here
      @user=User.new
  end
    
   

#Shows the profile for a user after logging in 
  def show
  	#if a request comes in to show a users profile page it comes here. From the id get the user
  	@user=User.find_by(id: params[:id])
  	#Show the page only if this user is the currently logged in user
  	if @user.nil? || @user!=current_user then
  		#if the user dosent exist or if he is not currently logged in we redirect him  to homepage
        @user=User.new
  		redirect_to home_path


  	end
    if current_user
        #summarise_updates_for_public_timeline 
    end
    
            
    respond_to do |format|
        format.html do 
            render "users/show"
        end
    end
  end
      


    


      #serves the POST op for  sign up andd HOME PAGE after login
  def create
  	#Create a model object using the input from the form. We cant use the params directly becoz of safety reasons.
  	#So we call a function which explicitely permits the operation
  	@user=User.new(user_params)
      
  	#if the signup is succeeded then we redirect him to homepage
  	if @user.save then 
  		#Commenting below login code becoz we dont want to login user just after signup. We will send a mail and user will have to activate his acciunt
  		log_in @user
  		
                
        #Todo: do not log him in yet. make him activate his account first
  		redirect_to @user
  	else
	  	#If the save failed for whatever reason(mostly becoz of validations,) then redirect him to new template(not new action).
		#here becoz the values are present in adminuser, it will be visible in the form
  		render 'new'
  	end

  end

#serves the POST op for  login
#called when login is done by admin or recruiter
  def login
    	#find the user by username. here session is the hash inside params coz that is what we specified in the form_for of login
  	user=User.find_by(username: params[:session][:username])
  	#if user is not admin check him in the user model
  	if user.nil?
  		user=User.find_by(username: params[:session][:username])
  	end
  	#if the user is present as well as password matches. authenticate api is provided by has_secure_password
  	if(user && user.authenticate(params[:session][:password]))
  		#call the log_in method inside admin_users_helper
  		log_in user
  		#this function will save id and token in browser and hashed token in db
  		remember user 
  		#redirect to profile page. Note again that modelobjectname serves the purpose of routing to appropriate page
      if user.admin?
        redirect_to '/admin/user_list'
      else
    		redirect_to show_my_profile_path
      end
  	else
  		#in case of unsuceesful login 
  		if !user 
  			#if there was no user then username was also wrong
  			flash[:danger]="Invalid username or password"
  		else
  			#user was present in which case only password mismatched
  			flash[:danger]="Invalid password"
  		end
      #I dont know right now which is the best page to redirect hi to. but taking hi to landing page now
      #redirect_to user_signup_path
      redirect_to root_path
  	end
  end

  #called when admins and recruiters logout
  def destroy
  	#call logout method in admin_helper but only if we r loggedin
  	log_out if logged_in?
  	#redirect him to homepage
  	redirect_to root_path
  end


  private 	
  		#This function simply guves a secure way to access params
  		def user_params#called as strong parameters
            params.require(:user).permit(:username,:email,:password,:password_confirmation)
  		end

end
