class ProfilesController < ApplicationController
	include UsersHelper
    include ProfilesHelper
    
    IMAGES_PERPAGE=20
    
    
	def show_my_profile



        if current_user
            if current_user.admin?
                redirect_to '/admin/users'
                return
            end

        	page_num= params[:page] ? params[:page] : 1
            @updates=current_user.get_updates "new", page_num
        else
            @user=User.new
        end
        
        

        respond_to do |format|
            format.html do 
                render "users/show"
            end
        end
        
    end





    def show_tabs
        id=params[:id]
        tab=params[:tab]
            @success=false

        @person=User.find_by(id:id)

        if current_user && current_user.id==@person.id
            page_num= params[:page] ? params[:page] : 1
           
            @updates=current_user.get_updates tab, page_num

            @success=true

        end

		respond_to do |format|
            format.html do 
                render "users/show"

            end
		end
    end

    def admin_view
        if current_user && current_user.admin?
            name= params[:name] 
            if name=="users"
                @type="users"
                @users=User.where(user_type: 0).limit(20);
            else
                @type="agents"
                @users=User.where(user_type: 1).limit(20);
            end
        end
        respond_to do |format|
            format.html do 
                render "users/show"
            end
        end
        
    end

 
    def user_suspend
        id= params[:id] 
        begin
            throw 'unauthorized' unless current_user.admin?
            @user=User.find_by(id:id)
            @user.toggle_suspension
            if @user.suspended?
                @text='Unblock'
            else
                @text='Block'
            end


        rescue Exception=>ex
            debugger
        end
        @type="user_suspend"
        respond_to do |format|
            format.js {render :file => "profiles/ajaxresponse/show_my_profile.js.erb" }
        end

    end

    def generate_report
        #pdf info taken from https://stackoverflow.com/questions/33150683/rails-wicked-pdf-convert-page-as-pdf
        @updates=current_user.get_updates "all", 0
        respond_to do |format|
            format.pdf do
                render pdf: "file_name.pdf"   # Excluding ".pdf" extension.
            end
        end
        
    end

end
