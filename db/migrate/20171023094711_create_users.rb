class CreateUsers < ActiveRecord::Migration[5.0]
  def up
    create_table :users do |t|
        t.string :username , :null=>false, unique: true
        t.string :name 
        t.string :email, :null=>false, unique: true
        t.string :profilepic
        t.integer :user_type,default:0
        t.integer :active,default:1 #profile suspended or active 0 is suspended


        t.string 'password_digest'
        t.string 'remember_digest'#this is to maintain login session through reboots
        
          
        t.timestamps null: false
    end
  end

  def down 
  	drop_table :users
  end
end
