class CreateComments < ActiveRecord::Migration[5.0]
	def up
    create_table :comments do |t|

        t.integer :ticket_id, null:false
        t.integer :user_id, null:false
        
        t.string :comment,limit:5000#this is the markdown version of the comment
        t.integer :actual_content_id, null:false#actualcontent willl have the real conrtent in case the user wants to edit
        
      t.timestamps null: false
    end
  end

  def down
    	drop_table :comments
  end
end
