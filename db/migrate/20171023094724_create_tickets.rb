class CreateTickets < ActiveRecord::Migration[5.0]

  def up
    create_table :tickets do |t|

        t.integer :created_by, null:false
        t.integer :assigned_to

        t.integer :state,default:0#0 is new, 1 is in progress, 2 is resolved
        
        t.string :heading,limit:1000

        
      t.timestamps null: false
    end
  end

  def down
    	drop_table :tickets
  end


end
