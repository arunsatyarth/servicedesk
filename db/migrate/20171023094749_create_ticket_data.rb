class CreateTicketData < ActiveRecord::Migration[5.0]
  def up
    create_table :ticket_data do |t|

        t.integer :ticket_id, null:false
        t.string :content,limit:5000

        t.integer :actual_content_id, null:false#actualcontent willl have the real conrtent in case the user wants to edit
        
      t.timestamps null: false
    end
  end

  def down
    	drop_table :ticket_data
  end
end
