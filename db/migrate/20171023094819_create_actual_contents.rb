class CreateActualContents < ActiveRecord::Migration[5.0]
  def up
    create_table :actual_contents do |t|
   		t.string :content, limit: 5000

      t.timestamps null: false
    end
  end

  def down
    	drop_table :actual_contents
  end
end
